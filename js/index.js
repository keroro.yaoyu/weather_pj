document.write('<script src="js/weatherState.js"></script>');
$(document).ready(function(){
    
    $.ajax({
        url:"http://opendata.cwb.gov.tw/api/v1/rest/datastore/F-C0032-001?Authorization=CWB-82DF11BB-4113-4CBE-BD54-1D3515080F2E&format=JSON",
        method:'GET',
        dataType:'json',
        data:'',
        async:true,
        success:function(data){
            console.log(data);

            for($i=0;$i<8;$i++){
                $location = data.records.location[$i].locationName;
                $state = data.records.location[$i].weatherElement[0].time[0].parameter.parameterName;;
                $rainrate = data.records.location[$i].weatherElement[1].time[0].parameter.parameterName;
                $temperature_L = data.records.location[$i].weatherElement[2].time[0].parameter.parameterName;
                $temperature_H = data.records.location[$i].weatherElement[4].time[0].parameter.parameterName;

                //col
                var cardCol = $("<div>");
                cardCol.addClass("col");

                //card
                var cardDiv = $("<div>");
                cardDiv.addClass("card opacity-50");
                

                //image
                // var cardIcon = $("<span>");
                // cardIcon.addClass("icon");
                // var cardImg = $("<img>");
                // cardImg.attr("src",weather($state));
                // cardImg.addClass("img-responsive");
                // cardIcon.append(cardImg);

                //card body
                var cardBody = $("<div>");
                cardBody.addClass("card-body");

                //name [需要修改]
                var cardLocat = $("<h5>");
                cardLocat.addClass("card-title");
                cardLocat.html("地區:"+$location);


                //age [需要修改]
                var cardState = $("<span>");
                cardState.html("天氣狀況:"+$state+"<br>");

                //country [需要修改]
                var cardTemperature = $("<span>");
                cardTemperature.html("溫度:"+$temperature_L+"~"+$temperature_H+"度"+"<br>");

                var cardrain = $("<span>");
                cardrain.html("降雨率:"+$rainrate+"%"+"<br>");
                
                var k = $("<br>");
                cardBody.append(
                    k,
                    cardLocat,
                    cardState,
                    cardTemperature,
                    cardrain,
                    k,
                );

                // [需要修改]
                cardDiv.append(cardBody); //cardIcon,cardBody

                // [需要修改]
                cardCol.append(cardDiv);

                $("#card-container").append(cardCol);
 

            }
            
        },
        erroe: function(){
            alert("request error!");
        },
    });
});

