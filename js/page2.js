$(document).ready(function(){
    
    $("#location").change(function(){
        var location = $("#location").val();
        //console.log(location);

        $.ajax({
            url:"http://opendata.cwb.gov.tw/api/v1/rest/datastore/F-C0032-001?Authorization=CWB-82DF11BB-4113-4CBE-BD54-1D3515080F2E&format=JSON",
            method:'GET',
            dataType:'json',
            data:'',
            async:true,
            success:function(data){
                console.log(data.records.location);

                for($i=0;$i<data.records.location.length;$i++){

                    if(location==data.records.location[$i].locationName){
                        $rainrate = data.records.location[$i].weatherElement[1].time[0].parameter.parameterName;
                        var temperature_L = data.records.location[$i].weatherElement[2];
                        var temperature_H = data.records.location[$i].weatherElement[4];
                        var time_part1 = temperature_H.time[0].startTime;
                        var time_part2 = temperature_H.time[1].startTime;
                        var time_part3 = temperature_H.time[2].startTime;

                        show_chart(temperature_L,temperature_H,time_part1,time_part2,time_part3);
                        config.update();
                    }
                }
            
               
            },
            error: function(){
                alert("request error!");
            },
        });
    });
    

    
});

function show_chart(temperature_L,temperature_H,time1,time2,time3){

    var time_part1 = time_split(time1).date+" "+time_split(time1).time;
    var time_part2 = time_split(time2).date+" "+time_split(time2).time;
    var time_part3 = time_split(time3).date+" "+time_split(time3).time;

    var config = {   
        type: 'line',
        data: {
            
            labels: [time_part1, time_part2, time_part3],
            datasets: [
                {
                    label: '時間段最高溫',
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    
                    data: [
                        parseInt(temperature_H.time[0].parameter.parameterName),
                        parseInt(temperature_H.time[1].parameter.parameterName),
                        parseInt(temperature_H.time[2].parameter.parameterName),
                    ],
                    fill: false,
                },
                {
                    label: '時間段最低溫',
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    
                    data: [
                        parseInt(temperature_L.time[0].parameter.parameterName),
                        parseInt(temperature_L.time[1].parameter.parameterName),
                        parseInt(temperature_L.time[2].parameter.parameterName),
                    ],
                    fill: false,                            
                },
            ],
        },
        options: {
            responsive: true,
            
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: '時間段'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: '溫度(℃)'
                    }
                }]
            }
        }
    };

    var ctx = document.getElementById('canvas').getContext('2d');
    var chart = new Chart(ctx, config);
}

function time_split(datetime){
    let arr = datetime.split(" ");

    var date_c = arr[0].split('-')[1]+"/"+arr[0].split('-')[2];
    var time_c = arr[1].split(':')[0]+":"+arr[1].split(':')[1];

    let result = {
        date: date_c,
        time: time_c
    }

    return result;
}