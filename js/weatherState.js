function weatherState($state){
    if($state=="晴天"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/01.svg";
    }else if($state=="晴時多雲"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/02.svg";
    }else if($state=="多雲時晴"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/03.svg";
    }else if($state=="多雲"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/04.svg";
    }else if($state=="多雲時陰"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/05.svg";
    }else if($state=="陰時多雲"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/06.svg";
    }else if($state=="陰天"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/07.svg";
    }else if($state=="多雲陣雨" || $state=="多雲短暫雨" || $state=="多雲短暫陣雨" || $state=="午後短暫陣雨" || $state=="短暫陣雨" || $state=="多雲時晴短暫陣雨" || $state=="多雲時晴短暫雨" || 
    $state=="晴短暫陣雨" ||$state=="短暫雨"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/08.svg";
    }else if($state=="多雲時陰短暫雨" || $state=="多雲時陰短暫陣雨"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/09.svg";
    }else if($state=="陰時多雲短暫雨" || $state=="陰時多雲短暫陣雨"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/10.svg";
    }else if($state=="雨天" || $state=="晴午後陰短暫雨" || $state=="晴午後陰短暫陣雨" || $state=="陰短暫雨" || $state=="陰短暫陣雨" || $state=="陰午後短暫陣雨"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/11.svg";
    }else if($state=="多雲時陰有雨" || $state=="多雲時陰陣雨" || $state=="晴時多雲陣雨" || $state=="多雲時晴陣雨"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/12.svg";
    }else if($state=="陰時多雲有雨" || $state=="陰時多雲有陣雨" || $state=="陰時多雲陣雨"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/13.svg";
    }else if($state=="陰有雨" || $state=="陰有陣雨" || $state=="陰雨" || $state=="陰陣雨" || $state=="陣雨" || $state=="午後陣雨" || $state=="有雨"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/14.svg";
    }else if($state=="陰有雨" || $state=="陰有陣雨" || $state=="陰雨" || $state=="陰陣雨" || $state=="陣雨" || $state=="午後陣雨" || $state=="有雨" ||
    $state=="陰時多雲有雨" || $state=="陰時多雲有雨" || $state=="陰時多雲有雨" || $state=="陰時多雲有雨"){
        return "https://www.cwb.gov.tw/V8/assets/img/weather_icons/weathers/svg_icon/day/14.svg";
    }

}